#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

:author: DIU-Bloc2 - Univ. Lille
:date: 2019, mai

Tris de listes

"""

import random

def compare_entier_croissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int)  
             * >0  si a est supérieur à b
             * 0 si a est égal à b
             * <0 si a est inférieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_croissant(1, 3) < 0
    True
    """
    return a-b

def compare_entier_décroissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int) 
             * >0 si a est inférieur à b
             * 0 si a est égal à b
             * <0 si a est supérieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_décroissant(1, 3) > 0
    True
    """
    return b-a
def compare_chaine_lexicographique(a,b):
    '''
    : param a : (str) une chaîne de caractères
    : param b : (str) une chaîne de caractères
    : return : (int)
                * 1 si a est supérieur à b dans l'ordre lexicographique
                * 0 si a est égal à b
                * -1 si a est inférieur à b
    : Exemples:
    
    >>> compare_chaine_lexicographique("a","b")
    1
    >>> compare_chaine_lexicographique("b","a")
    -1
    >>> compare_chaine_lexicographique("a","aa")
    1
    >>> compare_chaine_lexicographique("aaa","aab")
    1
    compare_chaine_lexicographique("aa","aab")
    1
    '''
    if a==b:
        return 0
    if a>b :
        return 1
    return -1

def compare_chaine_longueur(a,b):
    '''
    : param a : (str) une chaîne de caractères
    : param b : (str) une chaîne de caractères
    : return : (int)
                * 1 si a est plus long que b
                * 0 si a est de même longueur que b
                * -1 si a est plus court que b
    : Exemples:
    
    >>> compare_chaine_lexicographique("a","b")
    0
    >>> compare_chaine_lexicographique("a","ab")
    -1
    >>> compare_chaine_lexicographique("ab","a")
    1
    >>> compare_chaine_lexicographique("aa","aab")
    -1
    '''
    if len(a)==len(b) :
        return 0
    if len(a)>len(b) :
        return 1
    return -1


def est_trie(l, comp):
    """
    :param l: (type sequentiel) une séquence 
    :param comp: une fonction de comparaison
    :return: (bool) 
      - True si l est triée
      - False sinon
    :CU: les éléments de l doivent être comparables
    :Exemples:

    >>> est_trie([1, 2, 3, 4], compare_entier_croissant)
    True
    >>> est_trie([1, 2, 4, 3], compare_entier_croissant)
    False
    >>> est_trie([], compare_entier_croissant)
    True
    """
    i = 0
    res = True
    while res and i < len(l) - 1:
        res = comp(l[i], l[i+1]) <= 0
        i += 1
    return res

def cree_liste_melangee(n):
    """
    :param n: (int) Longueur de la liste à créer
    :return: une permutation des n premiers entiers
    :CU: n >= 0
    :Exemples:
        
    >>> l = cree_liste_melangee(5)
    >>> len(l)
    5
    >>> sorted(l) == [0, 1, 2, 3, 4]
    True
    """
    l = list(range(n))
    random.shuffle(l)
    return l


################################################
#                  TRI 1                       #
################################################


def tri_1(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_1(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    tri par comparaison pair à pair de 2 termes successifs tirés aléatoirement
    """
    while not est_trie(l, comp):
        i = random.randint(0,len(l)-2)
        if comp(l[i], l[i+1]) > 0:
            tmp = l[i]
            l[i] = l[i+1]
            l[i+1] = tmp

################################################
#                   TRI 2                      #
################################################

def tri_2(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_2(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    trie qui place par comparaison en début l'élément le plus 'petit' de la liste
    et qui réitère sur les listes restantes : tri par insertion
    """
    n = len(l)
    for i in range(1, n):
        tmp = l[i]
        k = i
        while k >= 1 and comp(tmp, l[k - 1]) < 0 :
            l[k] = l[k - 1]
            k = k - 1
        l[k] = tmp


################################################
#                   TRI 3                      #
################################################

def fais_quelque_chose(l, comp, d, f):
    p = l[d]
    ip = d
    for i in range (d+1, f):
        if comp(p, l[i]) > 0:
            l[ip] = l[i]
            l[i] = l[ip+1]
            ip = ip + 1
    l[ip] = p
    return ip

def tri_3(l, comp, d=0, f=None):
    """
    algorithme de tri rapide
    
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :param d: un entier >= 0 et < len(l) (0 par défaut)
    :param f: un entier <= len(l) ou None. None équivaut à len(l)
    :param comp: une fonction de comparaison
    :return : la liste l triée
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_3(l, compare_entier_croissant)
    [1, 1, 2, 3, 4, 5, 9]
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_3(l, compare_entier_décroissant)
    [9, 5, 4, 3, 2, 1, 1]
    >>> l = [ "a", "aaa", "aa", "ba", "ab" ]
    >>> tri_3(l, compare_chaine_lexicographique)
    ['a', 'aa', 'aaa', 'ab', 'ba']
    >>> l = [ "a", "aaa", "aa", "ba", "ab" ]
    >>> tri_3(l, compare_chaine_longueur)
    ['a', 'aa', 'ab', 'ba', 'aaa']
    """
    if comp == compare_chaine_longueur and est_trie(l, comp) == False : # pour obtenir un rangement par longueur et ordre lexicographique
        tri_3(l, compare_chaine_lexicographique) # on range par ordre lexicographique avant de ranger par ordre de longueur
        
    if f is None: f = len(l)
    if f - d > 1:
        ip = fais_quelque_chose(l, comp, d, f)
        tri_3(l, comp, d=d, f=ip)
        tri_3(l, comp, d=ip+1, f=f)
    
    return l


def tri_selection(l, comp):
    '''
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :return : la liste l triée selon le paramètre de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_selection(l, compare_entier_croissant)
    [1, 1, 2, 3, 4, 5, 9]
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_selection(l, compare_entier_décroissant)
    [9, 5, 4, 3, 2, 1, 1]
    
    >>> l = [ "a", "aaa", "aa", "ba", "ab" ]
    >>> tri_selection(l, compare_chaine_lexicographique)
    ['a', 'aa', 'aaa', 'ab', 'ba']
    
    >>> l = [ "a", "aaa", "aa", "ba", "ab" ]
    >>> tri_3(l, compare_chaine_longueur)
    ['a', 'aa', 'ab', 'ba', 'aaa']
    
    '''
    if comp == compare_chaine_longueur and est_trie(l, comp) == False : # pour obtenir un rangement par longueur et ordre lexicographique
        tri_3(l, compare_chaine_lexicographique) # on range par ordre lexicographique avant de ranger par ordre de longueur

    for position in range(0,len(l)) : # on parcourt les éléments de la liste
        extremum = l[position] # on mémorise la valeur de l'élément
        position_extremum = position # et sa position
        for i in range(position+1,len(l)) : # on parcourt le reste de la liste à l'exclusion de cet élément
            if comp(l[i], extremum) < 0 : # si la fonction de comparaison est vérifiée
                position_extremum = i # on mémorise position et valeur de l'extremum
                extremum = l[i]
        l[position_extremum] = l[position] # on permute l'élément initial et l'extremum
        l[position]=extremum
    return l
            
def test() :
    l1 = [ "a", "ab", "aaa"]
    l2 = [ "c","ba","cb","abc" ]
    fusion_triee(l1,l2, compare_chaine_longueur)
    
def fusion_triee(l1,l2,comp):
    '''
    :param l1: (list) une liste triée selon le critère de comparaison
    :param l2: (list) une liste triée selon le critère de comparaison
    :param comp: une fonction de comparaison
    :return : une liste l fusion des listes l1,l2 triée selon le paramètre de comparaison
    :CU: Les éléments  de l1,l2 doivent être comparables 
    :Effet de bord: les éléments des listes l1 et l2 sont supprimés au fur et à mesure pour
    passer dans une liste L
    :Exemples:
    
    >>> l1 = [1, 2, 3]
    >>> l2 = [1, 3, 4]
    >>> fusion_triee(l1,l2, compare_entier_croissant)
    [1, 1, 2, 3, 3, 4]
    
    >>> l1 = [6, 2, 1]
    >>> l2 = [5, 4, 3]
    >>> fusion_triee(l1,l2, compare_entier_décroissant)
    [6, 5, 4, 3, 2, 1]
    
    >>> l1 = [ "a", "aa", "aaa" ]
    >>> l2 = ["ab", "ba" ]
    >>> fusion_triee(l1,l2, compare_chaine_lexicographique)
    ['a', 'aa', 'aaa', 'ab', 'ba']
    
    >>>  l1 = [ "a", "ab", "aaa"]
    >>>  l2=["c","cb","ba","abc" ]
    >>> fusion_triee(l1,l2, compare_chaine_longueur)
    ['a', 'c', 'ab', 'ba', 'cb,'aaa',"abc"]
    
    '''
    longueur_chaine_attendue=len(l1)+len(l2)
    l=[] # liste temporaire qui va se contruire par fusion classée des listes l1 et L2
    while len(l)!= longueur_chaine_attendue :
        if len(l2)==0 :
            l.append(l1[0])
            l1.remove(l1[0])
        else :
            if len(l1)==0 :
                l.append(l2[0])
                l2.remove(l2[0])
            else :
                if (comp(l1[0], l2[0]) < 0) :
                    l.append(l1[0])
                    l1.remove(l1[0])
                else :
                    if comp==compare_chaine_longueur and comp(l1[0], l2[0])==0 :
                        if (compare_chaine_lexicographique(l1[0],l2[0])<0) :
                            l.append(l1[0])
                            l1.remove(l1[0])
                        else :
                            l.append(l2[0])
                            l2.remove(l2[0])
                    else :
                        l.append(l2[0])
                        l2.remove(l2[0])

    return l
def tri_fusion(l,comp):
    '''
    :param l: (list) une liste triée selon le critère de comparaison
    :param comp: une fonction de comparaison
    :return : une liste triée selon le paramètre de comparaison
    :CU: Les éléments  de l1,l2 doivent être comparables 
    :Effet de bord: la liste l est modifiée lors du traitement
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_selection(l, compare_entier_croissant)
    [1, 1, 2, 3, 4, 5, 9]
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_selection(l, compare_entier_décroissant)
    [9, 5, 4, 3, 2, 1, 1]
    
    >>> l = [ "a", "aaa", "aa", "ba", "ab" ]
    >>> tri_selection(l, compare_chaine_lexicographique)
    ['a', 'aa', 'aaa', 'ab', 'ba']
    
    >>> l = [ "a", "aaa", "aa", "ba", "ab" ]
    >>> tri_3(l, compare_chaine_longueur)
    ['a', 'aa', 'ab', 'ba', 'aaa']
    
    '''
    # on range les paires d'éléments de la liste selon la méthode de comparaison choisie
    for i in range(0,len(l),2) :
        if i+1<len(l) :    
            if (comp(l[i], l[i+1]) > 0) :
                temp = l[i]
                l[i]=l[i+1]
                l[i+1]=temp
    # on transforme la liste en une liste constituée des listes formées par les éléments
    for i in range(0,len(l)) :
        l[i]=[l[i]]
    # on tri et fussionne les paires de listes adjacentes
    while len(l)!=1 :
        for i in range(0,len(l)) :
            if i+1<len(l) :
                l[i]=fusion_triee(l[i],l[i+1],comp) # on remplace le 1° terme de la paire par la liste triée fusionnée
                l.remove(l[i+1]) # on suprrime le second terme
                i=i+1

    return l[0] # on retourne le seul élément de la liste qui est la liste triée


    
    